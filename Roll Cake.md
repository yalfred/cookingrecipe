
# Swiss roll 

https://www.youtube.com/watch?v=c-hNWudWc2s

* Baking tray: 11.5 x 13 inch

Other baking trays are OK too, as long as one side is longer than 11 inches. Please adjust the recipes if different sized tray is used. 

## 1st Egg mix

1. Egg yolk: 5
1. Oil: 70 g (1/3 cup + 1 tablespoon)
1. Milk: 60 g (1/4 cup)
1. **MIX**
1. Cake flour (Sifted): 90 g (1/2 cup + 2 tablespoons)
1. **MIX**

## 2nd Egg mix

1. Egg white
1. White vinegar: 5 g, 1 teaspoon
1. **MIX with machine**
1. big bubble, add sugar by 1/3 each time
1. Sugar: 55 g,  1/4 cup

## combine mix

1. combine mix 1/3 a time. (Use flip motion)

## Put mix into mold

1. (Preheat oven 320'F)
1. Flatten
1. drop bubble
1. bake 320'F 30mins
1. cool 15-20mins
1. baking sheet on top, FLIP
1. Peel
1. trim edges
1. Flip again
1. Roll a bit. Put in fridge for 15-20mins

## Cream

1. Freeze bowl and mixer for 10mins
1. 185g heavy cream.
1. Sugar 35g

## Add cream on cake

1. flatten both way, leave edge

## Fridge 3-4 hours.

* Done.