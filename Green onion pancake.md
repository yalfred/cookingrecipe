# Green Onion Pancake

## Ingredient (for 10 pieces)

* 600g (all purpose) flour
* 300g Hot water
* 100g room temp water
* Green onion (cut) (Dry!) Pointy end remove so it doesn't poke thru dough

## Steps

1. Hot water to flour. Mix a little. Until flaky.

1. (Add butter or lard for extra flagrance)

1. Add cool water (20%-30% depends on dough humidity)

1. Mix until 三光
    * Don't stick to hand
    * Don't stick to container
    * Dough looks shiny

1. Wrap wihtout air. Rest 30-60mins

1. Cut into pieces

1. Oil table so dough doesn't stick.

1. Roll flat into long shape. (~16" x 8"-12")

1. White pepper, salt

1. Add onion

1. Roll along the long side. Roll tight into long roll.
    If leaking, then twist it a little. 
    Shake pull it so it's a little longer.

1. Roll the long piece into snail shape. Tug tail in.

1. Rest 10 minutes

1. Press flat. Or roll flatter if wanted (Roll with plastic wrap)

1. (Frozen if needed)

1. Fry!


## Result

* Little green onion taste

* No layers

* too thick (Dad)
